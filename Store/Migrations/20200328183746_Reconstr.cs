﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Migrations
{
    public partial class Reconstr : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductPrices_Price_PriceId",
                table: "ProductPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreProduct_Products_ProductId",
                table: "StoreProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreProduct_Stores_StoreId",
                table: "StoreProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreProduct",
                table: "StoreProduct");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Price",
                table: "Price");

            migrationBuilder.RenameTable(
                name: "StoreProduct",
                newName: "StoreProducts");

            migrationBuilder.RenameTable(
                name: "Price",
                newName: "Prices");

            migrationBuilder.RenameIndex(
                name: "IX_StoreProduct_ProductId",
                table: "StoreProducts",
                newName: "IX_StoreProducts_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts",
                columns: new[] { "StoreId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Prices",
                table: "Prices",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPrices_Prices_PriceId",
                table: "ProductPrices",
                column: "PriceId",
                principalTable: "Prices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreProducts_Products_ProductId",
                table: "StoreProducts",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreProducts_Stores_StoreId",
                table: "StoreProducts",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductPrices_Prices_PriceId",
                table: "ProductPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreProducts_Products_ProductId",
                table: "StoreProducts");

            migrationBuilder.DropForeignKey(
                name: "FK_StoreProducts_Stores_StoreId",
                table: "StoreProducts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Prices",
                table: "Prices");

            migrationBuilder.RenameTable(
                name: "StoreProducts",
                newName: "StoreProduct");

            migrationBuilder.RenameTable(
                name: "Prices",
                newName: "Price");

            migrationBuilder.RenameIndex(
                name: "IX_StoreProducts_ProductId",
                table: "StoreProduct",
                newName: "IX_StoreProduct_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreProduct",
                table: "StoreProduct",
                columns: new[] { "StoreId", "ProductId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Price",
                table: "Price",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductPrices_Price_PriceId",
                table: "ProductPrices",
                column: "PriceId",
                principalTable: "Price",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreProduct_Products_ProductId",
                table: "StoreProduct",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_StoreProduct_Stores_StoreId",
                table: "StoreProduct",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Migrations
{
    public partial class _2903 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Count",
                table: "StoreProducts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Count",
                table: "StoreProducts");
        }
    }
}

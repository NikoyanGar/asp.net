﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Migrations
{
    public partial class AddAdressandCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Adress",
                table: "Stores",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Products",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adress",
                table: "Stores");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Products");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Store.Migrations
{
    public partial class rec : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "StoreProducts",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_StoreProducts_StoreId",
                table: "StoreProducts",
                column: "StoreId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts");

            migrationBuilder.DropIndex(
                name: "IX_StoreProducts_StoreId",
                table: "StoreProducts");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "StoreProducts");

            migrationBuilder.AddPrimaryKey(
                name: "PK_StoreProducts",
                table: "StoreProducts",
                columns: new[] { "StoreId", "ProductId" });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.Models;

namespace MhersStore.Controllers
{
    public class GroupController : Controller
    {
        StoreContext _dbContext;
        List<Group> _grups;
        public GroupController(StoreContext context)
        {
            _dbContext = context;
        }
        // GET: Group
        public ActionResult Index()
        {
            _grups = _dbContext.Groups.ToList();
            return View(_grups);
        }

        // GET: Group/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Group/Adding
        public ActionResult Add(string Name, string Details)
        {
            try
            {
                _dbContext.Groups.Add(new Group() { Name = Name, Details = Details });
                _dbContext.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Group/Update
        public ActionResult Update(int Id)
        {
            var group = _dbContext.Groups.FirstOrDefault(p => p.Id == Id);
            return View(group);
        }

        // POST: Group/Updape
        public IActionResult SaveUpdate(Group _group)
        {
            var group = _dbContext.Groups.FirstOrDefault(p => p.Id == _group.Id);
            group.Name = _group.Name;
            group.Details = _group.Details;
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        //caution dangerous
        [Authorize(Roles = "admin")]

        public ActionResult Delete(int Id)
        {
            var group = _dbContext.Groups.FirstOrDefault(g => g.Id == Id);
            _dbContext.Groups.Remove(group);
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.Models;
using Store.ViewModels;

namespace MhersStore.Controllers
{
    public class ProductController : Controller
    {
        StoreContext _dbContext;
        List<Product> _products;
        public ProductController(StoreContext dbContect)
        {
            _dbContext = dbContect;

        }
        public IActionResult Index()
        {
            _products = _dbContext.Products.Include(p => p.Group).ToList();
            return View(_products);
        }

     
        public IActionResult Update(int id)
        {
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == id);

            return View(product);
        }

        public IActionResult SaveUpdate(Product product)
        {
            var prod = _dbContext.Products.FirstOrDefault(p => p.Id == product.Id);
            prod.Name = product.Name;
            prod.Code = product.Code;
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
      
        public IActionResult Create()
        {
            var grups = _dbContext.Groups.ToList();
            var productGroup = new ProductGroup();
            productGroup.Groups = grups;
            return View(productGroup);
        }

        //todo:Create real Code for catch
        public IActionResult Add(string Name, string Code, int GroupId)
        {
            try
            {
                _dbContext.Products.Add(new Product() { Code = Code, GroupId = GroupId, Name = Name });
                _dbContext.SaveChanges();
                Response.StatusCode = 200;
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                return View();
            }
        }

        [Authorize(Roles = "admin")]
        public IActionResult Delete(int Id)
        {
            //_products not worked
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == Id);
            _dbContext.Products.Remove(product);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

      
    }
}
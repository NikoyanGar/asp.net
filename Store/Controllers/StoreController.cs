﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MhersStore.Models;
using MhersStore.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Store.Models;
using Store.ViewModels;

namespace Store.Controllers
{
    public class StoreController : Controller
    {
        StoreContext _dbContext;
        //todo:something with it..namespace uncorrect
        List<Store.Models.Store> _stores;
        public StoreController(StoreContext context)
        {
            _dbContext = context;
        }
        // GET: Group
        public ActionResult Index()
        {
            _stores = _dbContext.Stores.ToList();
            return View(_stores);
        }
        [Authorize(Roles = "admin")]

        public ActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "admin")]

        public ActionResult Add(string Name, string Adress)
        {
            try
            {
                _dbContext.Stores.Add(new Store.Models.Store() { Name = Name, Adress = Adress });
                _dbContext.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Update(int Id)
        {
            var store = _dbContext.Stores.FirstOrDefault(s => s.Id == Id);
            return View(store);
        }

        public IActionResult SaveUpdate(Store.Models.Store _store)
        {
            var store = _dbContext.Stores.FirstOrDefault(p => p.Id == _store.Id);
            store.Name = _store.Name;
            store.Adress = _store.Adress;
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        //caution dangerous
        [Authorize(Roles = "admin")]

        public ActionResult Delete(int Id)
        {
            var store = _dbContext.Stores.FirstOrDefault(s => s.Id == Id);
            _dbContext.Stores.Remove(store);
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }
        public ActionResult AddProducts(int Id)
        {

            StoreProductViewModel storeProduct = new StoreProductViewModel()
            {
                Name = _dbContext.Stores.FirstOrDefault(s => s.Id == Id).Name,
                StoreId = Id,
                Groups = _dbContext.Groups.ToList(),

            };
            return View(storeProduct);
        }
        public IActionResult SaveInStore(SaveProductStoreViewModel saveProductStoreViewModel)
        {
            var product = saveProductStoreViewModel.Product;
            var price = new Price { PurchasePrice = saveProductStoreViewModel.Price };
            var productPrice = new ProductPrice { Product = product, Price = price };
            var storeProduct = new StoreProduct
            {
                Product = product,
                Count = saveProductStoreViewModel.Count,
                StoreId = saveProductStoreViewModel.StoreId
            };

            _dbContext.Products.Add(product);
            _dbContext.Prices.Add(price);
            _dbContext.ProductPrices.Add(productPrice);
            _dbContext.StoreProducts.Add(storeProduct);

            _dbContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        [Authorize(Roles = "admin")]

        public IActionResult ViewProductsInStore(int Id)
        {

            var query = _dbContext.StoreProducts.Join(_dbContext.Products, st => st.ProductId, pr => pr.Id, (st, pr) => new
            {
                pr.Id,
                pr.Name,
                pr.Code,
                pr.GroupId,
                st.Count,
                st.StoreId

            }
            ).Where(s => s.StoreId == Id).Join(_dbContext.Groups, pr => pr.GroupId, g => g.Id, (pr, g) => new
            {
                pr.Id,
                pr.Name,
                pr.Code,
                pr.Count,
                pr.StoreId,
                GrupName = g.Name

            }
            ).Join(_dbContext.ProductPrices, prod => prod.Id, price => price.ProductId, (prod, price) => new
            {
                prod.Name,
                prod.Code,
                prod.GrupName,
                prod.Count,
                prod.StoreId,
                price.PriceId
            }
            ).Join(_dbContext.Prices, prod => prod.PriceId, price => price.Id, (prod, price) => new
            {
                prod.Name,
                prod.Code,
                prod.Count,
                prod.GrupName,
                price.PurchasePrice,
                prod.StoreId,
                price.SellingPrice

            }).ToList();

            List<ViewProductsInStore> views = new List<ViewProductsInStore>(query.Count);
            if (query.Count != 0)
            {
                foreach (var item in query)
                {
                    views.Add(new ViewProductsInStore()
                    {
                        StoreId = item.StoreId,
                        Code = item.Code,
                        Count = item.Count,
                        GroupName = item.GrupName,
                        Name = item.Name,
                        PurchasePrice = item.PurchasePrice,
                        SellingPrice = item.SellingPrice
                    });
                }
            }
            return View(views);
        }
        public IActionResult SellProductAtStore(int Id)
        {
            var query = _dbContext.Stores.Where(st => st.Id == Id)
                .Join(_dbContext.StoreProducts, st => st.Id, sp => sp.StoreId, (st, sp) => new
                {
                    StoreId = st.Id,
                    StoreName = st.Name,
                    sp.ProductId,
                    ProductCount = sp.Count,

                })
                .Join(_dbContext.Products, sp => sp.ProductId, p => p.Id, (sp, p) => new
                {
                    sp.StoreId,
                    sp.StoreName,
                    sp.ProductCount,
                    sp.ProductId,
                    ProductCode = p.Code,
                    ProductName = p.Name
                }).Join(_dbContext.ProductPrices, p => p.ProductId, pp => pp.ProductId, (p, pp) => new
                {
                    p.StoreId,
                    p.ProductId,
                    p.ProductName,
                    p.ProductCount,
                    p.ProductCode,
                    p.StoreName,
                    pp.PriceId,

                }).Join(_dbContext.Prices, prod => prod.PriceId, p => p.Id, (prod, p) => new
                {
                    prod.ProductId,
                    prod.StoreId,
                    prod.ProductCode,
                    prod.ProductCount,
                    prod.ProductName,
                    prod.StoreName,
                    p.SellingPrice,
                    p.PurchasePrice
                }).ToList();
            List<SellViewModel> sells = new List<SellViewModel>(query.Count);
            if (query.Count!=0)
            {
                foreach (var item in query)
                {
                    sells.Add(new SellViewModel()
                    {
                        ProductId=item.ProductId,
                        StoreId=item.StoreId,
                        Code = item.ProductCode,
                        Count = item.ProductCount,
                        Name = item.ProductName,
                        StoreName = item.StoreName,
                        PurchasePrice = item.PurchasePrice,
                        SellingPrice = item.SellingPrice
                    });
                }
            }
           
            return View(sells);
        }
        //todo:rec
        public IActionResult Sell(int ProductId,int StoreId,int Count,int SellingPrice)
        {
            //kam name u code ov..
            //SellingPrice hetagayum ogut hashvelu hamar
            //stugel vor minus chga count@
            var query = _dbContext.StoreProducts
              .FirstOrDefault(s => s.StoreId == StoreId && s.ProductId == ProductId && s.Count >= Count);
            if (query.Count!=0)
            {
                query.Count-=Count;
                _dbContext.SaveChanges();
                //texekacnel gorcarqi barehajox avarti masin
            }

            return RedirectToAction(nameof(Index));
        }

    }
}
﻿using MhersStore.Models;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MhersStore.ViewModels
{
    public class SaveProductStoreViewModel
    {
        public Product Product { get; set; }
        public int Price { get; set; }
        public int StoreId { get; set; }
        public int Count { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MhersStore.ViewModels
{
    public class SellViewModel
    {
        public string StoreName { get; set; }
        public string  Name { get; set; }
        public string  Code { get; set; }
        public int Count { get; set; }
        public int SellingPrice { get; set; }
        public int PurchasePrice { get; set; }
        public int StoreId { get; set; }
        public int ProductId { get; set; }

    }
}

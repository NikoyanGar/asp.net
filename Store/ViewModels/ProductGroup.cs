﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.ViewModels
{
    public class ProductGroup
    {
        public int GroupId { get; set; }
        public List<Group> Groups { get; set; }
        public string Name { get; set; }
        public string  Code { get; set; }
    }
}

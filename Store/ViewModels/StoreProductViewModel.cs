﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MhersStore.ViewModels
{
    public class StoreProductViewModel
    {
        public string Name { get; set; }
        public int StoreId { get; set; }
        public List<Group> Groups { get; set; }
    }
}

﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MhersStore.ViewModels
{
    public class ViewProductsInStore
    {
        public int StoreId { get; set; }
        public string  Name { get; set; }
        public string  Code { get; set; }
        public int PurchasePrice { get; set; }
        public int SellingPrice { get; set; }
        public string GroupName { get; set; }
        public int Count { get; set; }
    }
}

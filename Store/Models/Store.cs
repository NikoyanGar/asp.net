﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Store
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public List<StoreProduct> StoreProducts { get; set; }
        public Store()
        {
            StoreProducts = new List<StoreProduct>();
        }

    }
}

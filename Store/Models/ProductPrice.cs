﻿using MhersStore.Models;
using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class ProductPrice
    {
        public int Id { get; set; } 
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int PriceId { get; set; }
        public Price Price { get; set; }
    }
}

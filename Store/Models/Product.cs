﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Store.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public List<StoreProduct> StoreProducts { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public List<ProductPrice> productPrices { get; set; }
    
        public Product()
        {
            productPrices = new List<ProductPrice>();
            StoreProducts = new List<StoreProduct>();
        }
    }
}

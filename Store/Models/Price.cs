﻿using Store.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MhersStore.Models
{
    public class Price
    {
        public Price()
        {
            ProductPrices = new List<ProductPrice>();
        }

        public int Id { get; set; }
        public int PurchasePrice { get; set; }
        public int SellingPrice { get; set; }
        public List<ProductPrice> ProductPrices { get; set; }
    }
}
